FROM rocker/shiny:3.5.1

RUN apt-get update && apt-get install -y libcurl4-openssl-dev \
    libfftw3-dev libjpeg-dev libtiff-dev \
    libxml2-dev libssl-dev \
    libxml2-dev && rm -rf /var/lib/apt/lists/*

ADD ./etc/ /etc/shiny-server/
ADD ./app/ /srv/shiny-server/
ADD ./data /srv/shiny-server/data/

COPY ./packages.R packages.R

RUN Rscript packages.R
